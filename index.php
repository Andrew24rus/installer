<?php

use Installer\Installer;

require_once 'vendor/autoload.php';

$installer = new Installer();
$installer
    ->setRepositoryUrl('https://@bitbucket.org/wtolk/adfm.git')
    ->setRepositoryBranch('devel')
    ->setRepositoryIsPrivate(true)
    ->install();