<?php


namespace Installer\IO;


class IO
{
    public static function input(string $prompt, bool $required = false): string
    {
        $prompt .= ' ';

        $inputValue = readline($prompt);
        $isNullValue = strlen(trim($inputValue)) == 0;

        if ($isNullValue && $required) {
            $inputValue = self::input($prompt, $required);
        }

        return $inputValue;
    }

    public static function log(string $message)
    {
        echo $message . PHP_EOL;
    }

    public static function error(string $errorText, string $code = null)
    {
        $errorText = "\e[41m{$errorText}[Код ошибки: {$code}]\e[49m";

        self::log($errorText);
    }
}