<?php


namespace Installer;


use Throwable;

class InstallerException extends \Exception
{
    /**
     * @var string|null
     */
    protected $errorMessage = null;

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = $this->errorMessage . PHP_EOL . $message;

        parent::__construct($message, $code, $previous);
    }
}