<?php

namespace Installer\Composer;

use Installer\Composer\Exceptions\ComposerInstallException;
use Installer\Composer\Exceptions\ComposerUpdateException;
use Installer\Shell\Shell;

class Composer
{
    public static function install(array $args = [])
    {
        try {
            self::runComposer('install', $args);
        } catch (\RuntimeException $e) {
            throw new ComposerInstallException($e->getMessage(), $e->getCode());
        }
    }

    public static function update(array $args = [])
    {
        try {
            self::runComposer('update', $args);
        } catch (\RuntimeException $e) {
            throw new ComposerUpdateException($e->getMessage(), $e->getCode());
        }
    }

    private static function runComposer(string $command, array $args = [])
    {
        $args = array_merge([$command], $args);

        Shell::execute('php', 'composer.phar', $args);
    }
}