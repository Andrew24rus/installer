<?php

namespace Installer\Composer\Exceptions;

use Installer\InstallerException;

class ComposerInstallException extends InstallerException
{
    protected $errorMessage = 'Ошибка установки пакетов Composer';
}