<?php

namespace Installer\Composer\Exceptions;

use Installer\InstallerException;

class ComposerUpdateException extends InstallerException
{
    protected $errorMessage = 'Ошибка обновления пакетов Composer';
}