<?php


namespace Installer\Git;


use Installer\InstallerException;

class GitCloneException extends InstallerException
{
    protected $errorMessage = 'Не удалось склонировать репозиторий';
}