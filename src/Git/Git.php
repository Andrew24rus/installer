<?php


namespace Installer\Git;


use Installer\Shell\Shell;

class Git
{
    /**
     * @var string
     */
    private $branch = 'master';

    /**
     * @var string
     */
    private $repositoryUrl;

    public function __construct(string $repositoryUrl)
    {
        $this->repositoryUrl = $repositoryUrl;
    }

    public function setCredentials(string $login, string $password)
    {
        $pattern = "/https?:\/\/.*@/i";
        $replacement = "https://{$login}:{$password}@";

        $this->repositoryUrl = preg_replace($pattern, $replacement, $this->repositoryUrl, 1);

        return $this;
    }

    public function setBranch(string $branch)
    {
        $this->branch = $branch;

        return $this;
    }

    public function cloneRepository(string $destination)
    {
        $args = ['-b', $this->branch, $this->repositoryUrl, $destination];

        try {
            Shell::execute('git', 'clone', $args);
        } catch (\RuntimeException $e) {
            throw new GitCloneException($e->getMessage(), $e->getCode());
        }
    }
}