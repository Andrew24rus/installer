<?php

namespace Installer\Shell;

use Symfony\Component\Process\Process;

class Shell
{
    public static function getProcess(string $program, string $command, array $args = []): Process
    {
        $isWindows = defined('PHP_WINDOWS_VERSION_BUILD');

        $options = [
            'environment_variables' => $isWindows ? ['PATH' => getenv('PATH')] : [],
            'process_timeout'       => 3600,
        ];

        $process = new Process(array_merge([$program, $command], $args));
        $process->setEnv($options['environment_variables']);
        $process->setTimeout($options['process_timeout']);
        $process->setIdleTimeout($options['process_timeout']);

        return $process;
    }

    public static function execute(string $program, string $command, array $args = [])
    {
        $process = self::getProcess($program, $command, $args);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new \RuntimeException($process->getErrorOutput(), $process->getExitCode());
        }
    }
}