<?php

namespace Installer;


use Exception;
use Installer\Composer\Composer;
use Installer\Git\Git;
use Installer\IO\IO;

class Installer
{
    private $projectName;

    private $defaultProjectName = 'adfm';

    private $repository = [
        'url' => null,
        'branch' => 'master',
        'isPrivate' => false
    ];

    public function __construct()
    {
        $this->projectName = IO::input('Введите название проекта:', true);
    }

    public function getProjectName(): string
    {
        return $this->projectName ?: $this->defaultProjectName;
    }

    public function getProjectPath(): string
    {
        return './' . $this->getProjectName();
    }

    public function setRepositoryUrl(string $repositoryUrl): Installer
    {
        $this->repository['url'] = $repositoryUrl;

        return $this;
    }

    public function setRepositoryBranch(string $repositoryBranch): Installer
    {
        $this->repository['branch'] = $repositoryBranch;

        return $this;
    }

    public function setRepositoryIsPrivate(bool $isPrivate): Installer
    {
        $this->repository['isPrivate'] = $isPrivate;

        return $this;
    }

    public function install()
    {
        try {
            $this->cloneRepository();
            $this->installComposerPackages();

            IO::log('Установка завершена');
        } catch (Exception $e) {
            IO::error($e->getMessage(), $e->getCode());
        }
    }

    private function cloneRepository()
    {
        $destinationPath = realpath('./') . DIRECTORY_SEPARATOR . $this->getProjectName();

        IO::log('Клонируем репозиторий в папку ' . $destinationPath);

        $this->prepareGit()
             ->cloneRepository($destinationPath);

        IO::log('Репозиторий склонирован');
    }

    private function prepareGit(): Git
    {
        $git = new Git($this->repository['url']);
        $git->setBranch($this->repository['branch']);

        if ($this->repository['isPrivate']) {
            $git = $this->authenticateGit($git);
        }

        return $git;
    }

    private function authenticateGit(Git $git): Git
    {
        $login = IO::input('Введите логин:', true);
        $password = IO::input('Введите пароль:');

        $git = $git->setCredentials($login, $password);

        return $git;
    }

    private function installComposerPackages()
    {
        IO::log('Устанавливаем пакеты Composer');

        Composer::update(["--working-dir={$this->getProjectPath()}"]);

        IO::log('Пакеты Composer установлены');
    }
}